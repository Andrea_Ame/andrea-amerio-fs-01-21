import React from "react";
import SingleCard from "../SingleCard";
import { objCard } from "../../models/card";

export default class Card extends React.Component {
  constructor() {
    super();

    this.state = {
      cards: [],
    };
  }
  async componentDidMount() {
		const response = await fetch('http://bedrock.test/wp-json/wp/v2/posts');
    const cards = await response.json();
    console.log(cards);

    this.setState({
      cards: cards.map(card => objCard(card))
    });
  }

  render() {
    const card = this.state.cards.map((card) => (
      <SingleCard key= { card.id } card={ card }/>
      ))
    return (
      <div className="row text-start">
        { card }
      </div>
    );
  }
}
