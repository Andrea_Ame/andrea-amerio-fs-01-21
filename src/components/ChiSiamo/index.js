import React from 'react';
import SinglePage from "../SinglePage";
import { objPage } from "../../models/page";

class ChiSiamo extends React.Component {
  constructor() {
    super();

    this.state = {
      pages: [],
    };
  }

  async componentDidMount() {
		const response = await fetch('http://bedrock.test/wp-json/wp/v2/pages');
    const pages = await response.json();
    console.log(pages);

    this.setState({
      pages: pages.map(page => objPage(page))
    });
  }

  render() {
    const page = this.state.pages.map((page) => (
      <SinglePage key={ page.id } page={ page }/>
    ))
    return(
      <div className="row">
        <div className="text-start">
          <div className="container m-2">
          { page }
          </div>
        </div>
      </div>
    );
  }
}

export default ChiSiamo;