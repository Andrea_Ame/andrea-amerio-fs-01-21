import React from 'react';
import ButtonLink from '../ButtonLink';

class SingleCard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="d-flex justify-content-center col-md-4 mt-4">
        <div className="card">
          <div className="card-body">
            {/* <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/32877/logo-thing.png" className="card-img-top" alt="foto"/> */}
            <h5 className="card-title">{ this.props.card.title.rendered }</h5>
            <p className="card-text" dangerouslySetInnerHTML={{ __html:this.props.card.excerpt.rendered }}></p>
            <ButtonLink id= { this.props.card.id } />
          </div>
        </div>
      </div>
        
    )
  }
}

export default SingleCard;