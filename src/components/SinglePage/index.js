import React from 'react';

class SinglePage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
				<h1>{ this.props.page.title.rendered }</h1>
				<p dangerouslySetInnerHTML={{ __html:this.props.page.content.rendered }}></p>
			</div>
    )
  }
}

export default SinglePage;