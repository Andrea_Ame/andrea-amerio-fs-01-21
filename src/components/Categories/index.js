import React from 'react';
import { Link } from 'react-router-dom';

class Categories extends React.Component {
	constructor() {
    super();

    this.state = {
      categories: [],
    };
  }

	async componentDidMount() {
		const response = await fetch(`http://bedrock.test/wp-json/wp/v2/categories`);
    const categories = await response.json();
    console.log(categories);

    this.setState({
      categories,
		})
  }

  render() {
		const items = this.state.categories.map(cat => 
			<li className="nav-item" key={ cat.id }>
				<Link className="nav-link" to={`/categories/${ cat.id }/posts`}>{ cat.name }</Link>
			</li>)
    return(
      <>
				{ items }
			</>
    );
  }
}

export default Categories;