import React from "react";
import { withRouter } from "react-router";

class ReadPost extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      post: null,
    };
  }
  async componentDidMount() {
		const response = await fetch(`http://bedrock.test/wp-json/wp/v2/posts/${ this.props.match.params.id }`);
    const post = await response.json();
    console.log(post);

    this.setState({
      post,
    });
  }

  render() {
    return (
      <div className="text-start">
				<h1>
        { this.state.post?.title?.rendered }
				</h1>
				<p className="card-text" dangerouslySetInnerHTML={{ __html:this.state.post?.content?.rendered }} ></p>
			</div>
    );
  }
}

export default withRouter(ReadPost);