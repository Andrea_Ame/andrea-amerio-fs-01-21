import React from "react";
import { Link } from 'react-router-dom';

export default class ButtonLink extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div> 
          <Link to= {`/read-post/${this.props.id}`} className="nav-link">Read more</Link>
      </div>
    );
  }
}
