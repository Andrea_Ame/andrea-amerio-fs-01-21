import React from 'react';
import { withRouter } from "react-router";
import ButtonLink from '../ButtonLink';

let Variabile = false;

class Sezione extends React.Component {
    constructor(props){
    super(props)

    this.state = {
        post: [],
      };
    }

    async altraFunzione() {
      Variabile = true;
      const response = await fetch(`http://bedrock.test/wp-json/wp/v2/posts?categories=${ this.props.match.params.id }`);
      const post = await response.json();
      console.log(post);

      this.setState({
          post,
        });
        console.log(this.state.post);
    }
    
    componentDidMount(){
      this.altraFunzione();
    }

    componentDidUpdate(){
      if(Variabile == true){
        Variabile = false;
        return;
      }
      this.altraFunzione();
    }

  render() {
    const cards = this.state.post.map((article) => (
      <div key={ article.id } className="d-flex justify-content-center col-md-4 mt-4">
        <div className="card">
          <div className="card-body">
            <h5>{ article.title?.rendered }</h5>
            <p dangerouslySetInnerHTML={{ __html:article.excerpt?.rendered }}></p>
            <ButtonLink id={ article.id }/>
          </div>
        </div>
      </div>
    ))
    return(
      <div className="row text-start">
        { cards }
      </div>
    );
  }
}

export default withRouter(Sezione);