import './App.css';

import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';

import Home from './components/Home';
import NotFound from './components/NotFound';
import ChiSiamo from './components/ChiSiamo';
import ReadPost from './components/ReadPost';
import Categories from './components/Categories';
import Sezione from './components/Sezione';

function App() {
  return (
    <div className="row">
      <BrowserRouter>
        <header role="banner" className="text-center">
          <img id="logo-main" src="assets/images/logo.jpg" height="200" alt="logo"/>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <div className="container">
                <div className="collapse navbar-collapse" id="navbarNav">
                  <ul className="navbar-nav">
                    <li className="nav-item">
                      <Link to="/" className="nav-link">Home</Link>
                    </li>
                    <li className="nav-item">
                      <Link to="/chi-siamo" className="nav-link">Chi Siamo</Link>
                    </li>
                      <Categories />
                  </ul>
                </div>
              </div>
            </nav>
            <div className="container">
              <div className="col-lg-12">
                <Switch>
                  <Route path="/" exact>
                    <Home />
                  </Route>
                  <Route path="/chi-siamo">
                    <ChiSiamo />
                  </Route>
                  <Route path="/read-post/:id">
                    <ReadPost />
                  </Route>
                  <Route path="/categories/:id/posts">
                    <Sezione />
                  </Route>
                  <Route path="*">
                    <NotFound />
                  </Route>
                </Switch>
              </div>
            </div>
        </header>

        <footer className="row">
          <div className="container text-center bg-light p-2 mt-4">
            <div className="row">
              <div className="col-12">
                <p>Backend Wordpress + Frontend React</p>
              </div>
            </div>
          </div>
        </footer>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
