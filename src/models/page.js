export class Page {
	constructor(id, title, content) {
	  this.id = id;
	  this.title = title;
	  this.content = content;
	}
}

export function objPage(obj) {
	return new Page(obj.id, obj.title, obj.content);
}