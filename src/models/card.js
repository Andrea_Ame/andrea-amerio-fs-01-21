export class Card {
	constructor(id, title, content, excerpt) {
	  this.id = id;
	  this.title = title;
	  this.content = content;
	  this.excerpt = excerpt;
	}
}

export function objCard(obj) {
	return new Card(obj.id, obj.title, obj.content, obj.excerpt);
}